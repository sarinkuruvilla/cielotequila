jQuery(document).ready(function($) {

    $('a.our-story-menu-item').click(function() {
        $(window).scrollTo($('section.our-story') , 800);
    });

    $('a.whiskeys-menu-item').click(function() {
        $(window).scrollTo($('section.whiskeys') , 800);
    });

    $('a.accolades-menu-item').click(function() {
        $(window).scrollTo($('section.awards') , 800);
    });

    // Initiate bootstrap carousel
    $("#carousel").carousel();

    // Enable touch/swipe carousel
    $('#carousel').bcSwipe({ threshold: 25 });


    var domain = window.location.host;
    var current_url = window.location.pathname;
    var overlay = document.getElementById('overlay');
    var age_verify_pages = [''];
    var cookie = "cielorememberage";
    var age_redirect_url = "http://responsibility.org/";
    var remember_age = document.getElementById('remeber-age');
    var age_verify_yes = document.getElementById('age-verify-btn-yes');
    var age_verify_no = document.getElementById('age-verify-btn-no');
    var checkURL = function(domain, url, pages) {
        var url_paths = current_url.split('/');
        if (url_paths) {
            var end_path = url_paths[(url_paths.length - 2)];
            for (var j = 0; j < pages.length; j++) {
                if (end_path == pages[j]) {
                    return true;
                }
            }
        } else {
            return null;
        }
    };

    var rememberAgeCheck = function(checkbox, cookie, domain) {
        console.log("Running Age Check function...");
        if (checkbox.checked === true) {
            return true;
        } else {
            return false;
        }
    };
    var setOverLay = function() {
        if (checkURL(domain, current_url, age_verify_pages) && overlay) {
            setTimeout(function() {
                overlay.style.display = "block";
            }, 1000); // Wait 1 second
        }
    };
    var setAgeCookie = function(cookie, domain) {
        var d = new Date();
        d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cookie + "=true;" + expires + ";" + ";path=/;";
    };
    var getCookie = function(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
        }
        return "";
    };

    var checkCookie = function(cookie, cb) {
        var remeber_age_cookie = getCookie(cookie);
        if (remeber_age_cookie != "true") {
            cb();
        }
    };
    checkCookie(cookie, setOverLay);

    age_verify_yes.onclick = function(e) {
        e.preventDefault();
        overlay.style.display = "none";
        if (rememberAgeCheck(remember_age, cookie, domain)) {
            setAgeCookie(cookie, domain);
        }
    };
    age_verify_no.onclick = function(e) {
        e.preventDefault();
        window.location.href = age_redirect_url;
    };

});